defmodule Yarn do
  require HTTPoison
  require Poison
  @spec login(any, any, any) :: <<_::16, _::_*8>>
  def login(username, password, yarnpod) do
    "#{username}_#{password}_#{yarnpod}"
  end
  @spec api_endpoint(any) :: <<_::64, _::_*8>>
  def api_endpoint(yarnpod) do
    "https://#{yarnpod}/api/v1"
  end

  @spec get_jwt_token(any, any, any) :: any
  def get_jwt_token(username, password, endpoint) do
    body = %{username: username, password: password}
    headers = %{"Content-Type" => "application/json"}

    {:ok, response} = HTTPoison.post("#{endpoint}/auth", Poison.encode!(body), headers)
    response_body = Poison.decode!(response.body)

    response_body["token"]
  end
end
