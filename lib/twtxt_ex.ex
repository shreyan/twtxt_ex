defmodule Twtxt do
  @moduledoc """
  A library for working with local Twtxt files.
  """

  @spec read_file(
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | char,
              binary | []
            )
        ) :: {:error, atom}
  @doc """
  Read a Twtxt file and return a list of entries.
  """
  def read_file(path) do
    with {:ok, file} <- File.open(path, [:read]), do: file
      |> IO.read(:all)
      |> String.split("\n")
      |> Enum.reject(&(&1 == ""))
  end

  def list_entries(file) do
    Twtxt.read_file(file) |> Enum.map(&Twtxt.parse_entry/1)
  end

  @spec write_file(
          binary
          | maybe_improper_list(
              binary | maybe_improper_list(any, binary | []) | byte,
              binary | []
            ),
          any
        ) :: :ok | {:error, atom}
  @doc """
  Write a list of entries to a Twtxt file.
  """
  def write_file(path, entries) do
    entries
    |> Enum.map(&to_string/1)
    |> Enum.join("\n")
    |> File.write(path)
  end

  @spec parse_entry(binary) :: %{date: binary, text: binary}
  @doc """
  Parse a Twtxt-formatted entry into a map.
  """
  def parse_entry(entry) do
    [date, text] = String.split(entry, "\t")
    %{date: date, text: text}
  end


  @spec filter_by_date(any, any, any) :: list
  @doc """
  Filter a list of entries by date.
  """
  def filter_by_date(entries, start_date \\ Date.new(0, 1, 1), end_date \\ Date.utc_today()) do
    entries
    |> Enum.filter(fn entry ->
         date = Date.from_iso8601!(entry[:date])
         start_date <= date and date <= end_date
       end)
  end

  @doc """
  Sort a list of entries by date.
  """
  def sort_by_date(entries) do
    entries
    |> Enum.sort_by(fn entry -> Date.from_iso8601!(entry[:date]) end)
  end

  @doc """
  Limit the number of entries in a list.
  """
  def limit_entries(entries, limit) do
    entries |> Enum.take(limit)
  end

  @spec add_entry(any, any) :: none
  @doc """
  Add a new entry to a list of entries.
  """
  def add_entry(entries, text) do
    date = DateTime.utc_now() |> DateTime.to_iso8601()
    %{date: date, text: "\t#{text}"} |> List.insert_at(entries, 0)
  end

  @spec edit_entry(any, any, any) :: none
  @doc """
  Edit an existing entry in a list of entries.
  """
  def edit_entry(entries, index, text) do
    entry = entries |> Enum.at(index)
    date = entry[:date]
    %{date: date, text: text} |> List.replace_at(entries, index)
  end
end
